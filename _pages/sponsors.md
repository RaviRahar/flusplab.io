---
title: "Sponsors"
---

Here is a list of organizations that help us keep FLUSP's dream alive. We thank
all those companies and institutions immensely for relying on our work and
encouraging us to keep moving forward.

### Events' Sponsors
{:.title-with_separator}

<div class="text-center">
  <a href="https://www.digitalocean.com/" class="spoonmsor_logo">
    <img src="/img/sponsors/DO.svg" width="251px">
  </a>
  <a href="https://www.analog.com/en/index.html/" class="spoonmsor_logo">
    <img src="/img/sponsors/ADI.png">
  </a>
  <a href="https://profusion.mobi/" class="spoonmsor_logo">
    <img src="/img/sponsors/ProFusion.png" width="251px">
  </a>
  <a href="https://www.collabora.com/" class="spoonmsor_logo">
    <img src="/img/sponsors/Collabora.png" width="185px">
  </a>
</div>

### Donors and Supporters
{:.title-with_separator}

<div class="text-center">
  <a href="https://www.linaro.org/" class="spoonmsor_logo">
    <img src="/img/sponsors/linaro.png" width="200px">
  </a>
  <a href="https://www.digitalocean.com/" class="spoonmsor_logo">
    <img src="/img/sponsors/DO.svg" width="251px">
  </a>
  <a href="https://www5.usp.br/#english" class="spoonmsor_logo">
    <img src="/img/sponsors/USP.jpg" width="200px">
  </a>
  <a href="https://www.analog.com/en/index.html/" class="spoonmsor_logo">
    <img src="/img/sponsors/ADI.png">
  </a>
</div>
