---
title:  "EVAL-SDP-CB1Z"
image:  "EVAL-SDP-CB1Z"
ref: "resource"
categories: "resources.md"
id: 4
---

The SDP-B is a controller board for the System Demonstration Platform (SDP).
The SDP-B is based on the Blackfin ADSP-BF527 processor with connectivity to
the PC through a USB 2.0 high speed port. Controller boards allow for the
configuration and capture of data on daughter boards from the PC via USB.

### URLs

* [EVAL-SDP-CB1Z](https://www.analog.com/en/design-center/evaluation-hardware-and-software/evaluation-boards-kits/SDP-B.html)
* [User Guide](https://www.analog.com/media/en/technical-documentation/user-guides/UG-277.pdf)
