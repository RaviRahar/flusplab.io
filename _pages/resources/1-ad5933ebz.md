---
title:  "EVAL-AD5933EBZ"
image:  "EVAL-AD5933EBZ"
ref: "resource"
categories: "resources.md"
id: 1
---

The EVAL-AD5933EBZ evaluation board, and the application software developed to
interface with the device. The AD5933 is a high precision impedance converter
system that combines an on-board frequency generator with a 12-bit, 1 MSPS
analog-to-digital converter (ADC). The frequency generator allows an external
complex impedance to be excited with a known frequency. The on-board ADC
samples the response signal from the impedance, and an on-board DSP engine at
each excitation frequency processes the DFT. The AD5933 also contains an
internal temperature sensor with 13-bit resolution. The part operates from a
2.7 V to 5.5 V supply. Other on-board components include a ADR423 3.0 V
reference to act as a stable supply voltage for the separate analog and digital
sections of the device and a ADP3303 ultrahigh precision regulator to act as a
supply to the on-board universal serial bus controller that interfaces to the
AD5933. The user has the option to power the entire circuitry from the USB port
of a computer.

### URLs

* [EVAL-AD5933](https://www.analog.com/en/design-center/evaluation-hardware-and-software/evaluation-boards-kits/eval-ad5933.html)
* [User Guide](https://www.analog.com/media/en/technical-documentation/user-guides/UG-364.pdf)
