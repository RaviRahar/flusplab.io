var slideIndex = []; //stores the index of the displayed image for each slideshow
var slideId = []; //stores the css class for the images of each slideshow

window.onload = function(e){ 
    var slideDivs = document.querySelectorAll("div[data-id]");
    for (let item of slideDivs ) {
        var id = item.attributes['data-id'].nodeValue;
        slideId[id] = "slideShowImg" + id;
        slideIndex[id] = 1;
        showDivs(1, id);
    }
}

function plusDivs(n, no) {
  showDivs(slideIndex[no] += n, no);
}

function showDivs(n, no) {
  var i;
  var slideImgs = document.getElementsByClassName(slideId[no]);
  var slideCapts = document.getElementsByClassName("slideShowCapt" + no);
  if (n > slideImgs.length) {slideIndex[no] = 1}
  if (n < 1) {slideIndex[no] = slideImgs.length}
  for (i = 0; i < slideImgs.length; i++) {
    slideImgs[i].style.display = "none";  
    if (slideCapts.length > 0) {
      slideCapts[i].style.display = "none";
    }
  }
  slideImgs[slideIndex[no]-1].style.display = "block";  
  if (slideCapts.length > 0) {
    slideCapts[slideIndex[no]-1].style.display = "block";  
  }
}
